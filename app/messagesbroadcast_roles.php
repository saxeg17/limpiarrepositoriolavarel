<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class messagesbroadcast_roles extends Model
{
    public $table = "emqu_messagesbroadcast_roles";

    protected $fillable = ['id_emqu_role', 'id_emqu_messagesbroadcast'];

}
