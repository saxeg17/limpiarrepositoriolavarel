<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
class VerifyAccessKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Obtenemos el api-key que el usuario envia
        $key = $request->headers->get('api_key');
        
        if(isset($key)){
            $user = User::where('api_token', $key)
                ->first();

            if ($user == null){
                return response()->json(['error' => 'no autorizado' ], 401); 
            }else{
                return $next($request);
            }
        }else{
            return response()->json(['error' => 'no autorizado' ], 401); 
        }
        
    }
}
