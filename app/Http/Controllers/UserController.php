<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Mail\Message;
use App\Mail\EmailPasswd as EmailPasswd;
use App\Mail\EmailNewUser as NewUserEmail;
class UserController extends Controller
{
    /**
       * Display a listing of the resource.
       *
       * @return Response
       */
      public function index()
      {
        $user = User::where('api_token', Request::header('api-key'))
                ->first();
        
          if($user->id_emqu_role  == 2 || $user->id_emqu_role  == 1) {
             return response()->json(
                User::all()
            );
          }else{
            return ['error' => 'el rol no tiene permisos'];
          }
         
      }

      public function store(Request $request)
      {
        
          $user = User::where('api_token', Request::header('api-key'))
                ->first();
        
          if($user->id_emqu_role  == 1 || $user->id_emqu_role == 2) {
              if (!is_array(Request::all())) {
                  return ['error' => 'request must be an array'];
              }
              // Creamos las reglas de validación
              $rules = [
                  'firstname' => 'required',
                  'lastname' => 'required',
                  'position' => 'required',
                  'id_emqu_role' => 'required',
                  'id_emqu_company' => 'required',
                  'email' => 'required|email',
                  'password' => 'required'
              ];

              try {
                  // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
                  $validator = \Validator::make(Request::all(), $rules);
                  if ($validator->fails()) {
                      return [
                          'created' => false,
                          'errors' => $validator->errors()->all()
                      ];
                  }

                  User::create([
                      'name' => Request::input('firstname'),
                      'firstname' => Request::input('firstname'),
                      'lastname' => Request::input('lastname'),
                      'position' => Request::input('position'),
                      'id_emqu_role' => Request::input('id_emqu_role'),
                      'login' => false,
                      'id_emqu_company' => Request::input('id_emqu_company'),
                      'email' => Request::input('email'),
                      'api_token' => str_random(60),
                      'password' => bcrypt(Request::input('password')),
                  ]);

                  $mensajeSend = "Welcome to Tportal";
                  $asuntoSend = "Welcome to Tportal";
                  $body1s = "Welcome to Tportal";
                  $field1s = Request::input('email');
                  $field2s = Request::input('password');
                  $title1s = "New user credentials";

                  Mail::to(Request::input('email'),'New User Emqu')
                      ->send(new NewUserEmail($mensajeSend,$asuntoSend,$body1s,$field1s,$field2s,$title1s));

                  return ['created' => true];
              } catch (Exception $e) {
                  \Log::info('Error creating user: ' . $e);
                  return \Response::json(['created' => false], 500);
              }
          }
          else{
              return ['created' => false,
                      'error' => 'No tiene permisos para crear usuarios'];
          }
          
          
      }

      public function update(Request $request, $id)
      {
       
       
        $user = User::where('id', $id)->get();
         

        if (!$user->isEmpty()){
            $passwd = Request::input('password');
            if($passwd != ""){
                Request::merge(['password' => bcrypt(Request::input('password'))]); 
            }
            $firstUser = $user->first();
            $firstUser->update(Request::all());
            return ['updated' =>true]; 
        }else{
            return ['error' =>"no exist"];
        }
        //Input::has('price')
        
      }

      public function show($id)
      {
        return $id;
          $user = User::where('email', $id)
                ->first();
          return \Response::json($user);
      }
      public function showData()
      {
        
          $user = User::where('api_token', Request::header('api-key'))
                ->first();
          $respuesta = DB::select(DB::raw("SELECT u.*, r.role_name, c.name FROM users u, emqu_roles r, emqu_company c WHERE u.id = $user->id AND u.id_emqu_role = r.id AND u.id_emqu_company = c.id"));
          return \Response::json($respuesta);
      }
      

      /**
       * Show the form for creating a new resource.
       *
       * @return Response
       */
      public function create()
      {
          //
      }
      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function destroy($id)
      {

        $user = User::where('id', $id)->get();

        if (!$user->isEmpty()){
           User::destroy($id);
           return ['deleted' => true];
        }else{
            return ['error' =>"no exist"];
        }
      }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
        public function isLogin()
        {
            $model = User::where('api_token', Request::header('api-key'))
                ->first();

            if($model->login === 1){
                return ['login' => true];
            }else{
                 return ['login' => false];
            }            
        }

        public function showUser($id)
        {

          $user = User::where('api_token', Request::header('api-key'))
                ->first();
        
          if($user->id_emqu_role  == 1 || $user->id_emqu_role == 2) {
              $user = User::where('id', $id)->get();
              if (!$user->isEmpty()){
                 return response()->json(
                      $user
                  );
              }else{
                  return ['error' =>"no exist"];
              }
          }else{
              return ['error' => 'el rol no tiene permisos'];
          }

        }

      public function changePasswd(Request $request)
      {
        $user = User::where('api_token', Request::header('api-key'))
                ->first();
         
        $passwd = Request::input('password');
        $password_old = Request::input('password_old');
        if($passwd != ""&&$password_old !=""){
           
                $user = User::where('api_token', Request::header('api-key'))->first();
                $testb = false;
                $testb = password_verify(Request::input('password_old'), $user->password);
                if($testb){
                    Request::merge(['password' => bcrypt(Request::input('password'))]);
                    $user->update(Request::all());
                    return ['updated' =>true]; 
                }else{
                  return ['error' =>"password_old is wrong"];
                }

        }else{
            return ['error' =>"password and password_old is requerid"];
        }
      }
      public function recoveryPasswd(Request $request)
      {
        $user = User::where('email', Request::input('email'))->get();
        if (!$user->isEmpty()){
            $firstUser = $user->first();
            $code = substr(str_shuffle("123456789abcdefghijklmnpqrstuvwxyz"), 0, 6);           
            $title1s = Request::input('title1');
            $body1s = Request::input('body1');
            //$link1s = Request::input('link1');
            $body2s = Request::input('body2');
            if($title1s == ""){
              $title1s = "";
            }
            if($body1s == ""){
              $body1s = "";
            }
            /*
            if($link1s == ""){
              $link1s = "";
            }
            */
            if($body2s == ""){
              $body2s = "";
            }

            Request::merge(['remember_token' => $code]); 
            $firstUser->remember_token = $code;
            $firstUser->save();            
            Mail::to(Request::input('email'),'T-Portal')
            ->send(new EmailPasswd($code,"Recupera tu clave",$title1s,$body1s,$body2s));                    
            return ['code' =>true]; 
        }else{
            return ['error' =>"no exist"];
        }
      }
      public function recoveryPasswdByCode(Request $request)
      {

        $email = Request::input('email');
        $code = Request::input('code');
        if($email != ""&&$code != ""){
            $user = User::where('email', $email)
                          ->where('remember_token', $code)
                               ->get();

            if (!$user->isEmpty()){
                return ['response' =>true];                
            }else{
                return ['response' =>false];
            } 
        }else{
          return ['error' =>"Missing fields"];
        }
        
      }
      public function recoveryChangePasswd(Request $request)
      {
        $email = Request::input('email');
        $code = Request::input('remember_token');
        $passwd = Request::input('password');
        if($email != ""&&$code != ""&&$passwd != ""){         
            $user = User::where('email', $email)
                          ->where('remember_token', $code)
                               ->get();
            if (!$user->isEmpty()){
                Request::merge(['password' => bcrypt(Request::input('password'))]);   
                $firstUser = $user->first();
                $firstUser->update(Request::all());
                return ['updated' =>true];                 
            }else{
                return ['response' =>"Fields are wrong"];
            } 
        }else{
            return ['error' =>"Missing fields"];
        }
        
      }

}
