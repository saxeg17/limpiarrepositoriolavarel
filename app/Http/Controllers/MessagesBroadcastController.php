<?php

namespace App\Http\Controllers;

use App\messagesbroadcast_roles;
use Illuminate\Support\Facades\Request;
use App\User;
use App\messagesbroadcast;
use Auth;
use DB;
use Carbon\Carbon;

class MessagesBroadcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('api_token', Request::header('api-key'))
            ->first();

        if (!is_array(Request::all())) {
            return ['error' => 'request must be an array'];
        }

        // Creamos las reglas de validación
        $rules = [
            'title' => 'required',
            'message' => 'required',
            'type' => 'required',
            'icon' => 'required',
        ];

        try {
            // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
            $validator = \Validator::make(Request::all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors' => $validator->errors()->all()
                ];
            }

            $roles = Request::input('rol');

            $Message = messagesbroadcast::create([
                'title' => Request::input('title'),
                'message' => Request::input('message'),
                'type' => Request::input('type'),
                'icon' => Request::input('icon'),
                'dias' => Request::input('dias'),
                'user_id' => $user->id,
            ]);

            foreach ($roles as $rol){
                messagesbroadcast_roles::create([
                    'id_emqu_role' => $rol,
                    'id_emqu_messagesbroadcast' => $Message->id,
                ]);
            }

            return ['created' => true];
        } catch (Exception $e) {
            \Log::info('Error creating message: ' . $e);
            return \Response::json(['created' => false], 500);
        }

    }

    public function allForUser(Request $request)
    {
        $user = User::where('api_token', Request::header('api-key'))
            ->first();

        $today = "'".Carbon::now()."'";

        $response = DB::select(DB::raw("select m.*, $today as date
                FROM emqu_messagesbroadcast m, emqu_messagesbroadcast_roles mr, emqu_roles r, users u
                WHERE u.id = $user->id AND u.id_emqu_role = r.id AND r.id = mr.id_emqu_role AND mr.id_emqu_messagesbroadcast = m.id
                AND m.created_at > DATE_SUB($today,INTERVAL m.dias DAY)
                ORDER BY m.created_at desc LIMIT 5"));

        return $response;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('api_token', Request::header('api-key'))
            ->first();

        $response = DB::select(DB::raw("select m.*     
                FROM emqu_messagesbroadcast m, emqu_messagesbroadcast_roles mr, emqu_roles r, users u
                WHERE u.id = $user->id AND u.id_emqu_role = r.id AND r.id = mr.id_emqu_role AND mr.id_emqu_messagesbroadcast = m.id AND m.id = $id"));

        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
