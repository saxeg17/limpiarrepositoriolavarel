<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\User;
use Auth;
class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function changeEmailCredentials()
    {
        return view('admin.change-email-credentials');
    }

    public function editEmail(Request $request){
        $user = User::where('api_token', Request::header('api-key'))
            ->first();

        if($user->id_emqu_role == 2 || $user->id_emqu_role == 5) {
            if (!is_array(Request::all())) {
                return ['error' => 'request must be an array'];
            }

            $rules = [
                'mail_username' => 'required',
                'mail_password' => 'required',
            ];

            try {
                $validator = \Validator::make(Request::all(), $rules);
                if ($validator->fails()) {
                    return [
                        'edit' => false,
                        'errors' => $validator->errors()->all()
                    ];
                }

                $env_update = $this->changeEnv([
                    'MAIL_USERNAME'   => Request::input('mail_username'),
                    'MAIL_PASSWORD'   => Request::input('mail_password'),
                ]);
                if($env_update){
                    return ['edit mail credentials' => true];
                }

            } catch (Exception $e) {
                \Log::info('Error edit mail credential: ' . $e);
                return \Response::json(['edit' => false], 500);
            }
        }
        else{
            return ['edit' => false,
                'error' => 'No tiene permisos para editar credenciales de email'];
        }
    }

    public function edit(Request $request)
    {
        $env_update = $this->changeEnv([
            'MAIL_USERNAME'   => $request->mail_username,
            'MAIL_PASSWORD'   => $request->mail_password,
        ]);
        if($env_update){
            // Do something
        } else {
            // Do something else
        }

        return redirect('/changeEmailCredentials')->with('success', 'Información editada correctamente');
    }

    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);;

            // Loop through given data
            foreach((array)$data as $key => $value){

                // Loop through .env-data
                foreach($env as $env_key => $env_value){

                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);

            return true;
        } else {
            return false;
        }
    }
}
