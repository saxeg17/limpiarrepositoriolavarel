<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\emqu_company;
class CompanyController extends Controller
{
	
    public function index()
    {
        return emqu_company::all();
    }
    public function store(Request $request){
    	emqu_company::create($request->all());
    	return ['created' => true];;
    }
    public function update(Request $request, $id)
    {
        $company = emqu_company::find($id);
        $company->update($request->all());
        return ['updated' => true];
    }
    public function show($id)
    {
        return emqu_company::find($id);
    }
    public function destroy($id)
    {
        emqu_company::destroy($id);
        return ['deleted' => true];
    }
}
