<?php

namespace App\Http\Controllers;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Mail\Message;
use App\Mail\EmailTicket as TicketEmail;

use Illuminate\Support\Facades\Request;

class SendEmails extends Controller
{
    public function send(Request $request)
      {
      	if (!is_array(Request::all())) {
	          return ['error' => 'request must be an array'];
	      }
      	$rules = [
          'email' => 'required|email',
          'mensaje' => 'required',
          'asunto' => 'required'
        ];
        $validator = \Validator::make(Request::all(), $rules);
        if ($validator->fails()) {
	          return [
	              'email' => false,
	              'errors' => $validator->errors()->all()
	          ];
	      }
      	$emailSend = Request::input('email');
      	$mensajeSend = Request::input('mensaje');
      	$asuntoSend = Request::input('asunto');

        $body1s = Request::input('body1');
        $field1s = Request::input('field1');
        $field2s = Request::input('field2');
        $title1s = Request::input('title1');
        if($body1s == ""){
          $body1s = "";
        }
        if($field1s == ""){
          $field1s = "";
        }
        if($field2s == ""){
          $field2s = "";
        }
        if($title1s == ""){
          $title1s = "";
        }
       // return $body1s." ".$field1s." ".$field2s." ".$title1s;
        
        Mail::to($emailSend,'Usuario Emqu')
        ->send(new TicketEmail($mensajeSend,$asuntoSend,$body1s,$field1s,$field2s,$title1s));
        return ['email' => true];
        
      }
}
