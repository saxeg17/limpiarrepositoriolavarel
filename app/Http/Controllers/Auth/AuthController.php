<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Auth Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    public function postLogin(Request $request){

            if ($this->attemptLogin($request)) {
                $model = User::where('email', $request->email)
                    ->first();
                $model->login = true;
                $model->save();
                return response()->json($model);
            }

            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            return response()->json([
                'error' => 'Not Authorized!',
            ], 401);
    }

    public function getLogout(){

        $model = User::where('id', Auth::guard('api')->id())
            ->first();
        $model->login = false;
        $model->save();

        return response()->json([
            'logout' => true,
        ]);

    }

}
