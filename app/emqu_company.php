<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class emqu_company extends Model
{
    public $table = "emqu_company";
    
    protected $fillable = ['name'];
}
