<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class messagesbroadcast extends Model
{
    public $table = "emqu_messagesbroadcast";

    protected $fillable = ['title', 'message', 'type', 'icon', 'dias', 'user_id'];

    protected $hidden = ['user_id'];
}
