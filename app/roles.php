<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    public $table = "emqu_roles";

    protected $fillable = [
        'role_name'
    ];
}
