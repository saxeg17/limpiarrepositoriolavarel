<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailPasswd extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $mensaje;
    public $asunto;
    public $title;
    public $body;
    //public $link;
    public $body2;
    public function __construct($mensaje, $asunto, $title, $body/*, $link*/, $body2)
    {
        //
        $this->mensaje = $mensaje;
        $this->asunto = $asunto;
        $this->title = $title;
        $this->body = $body;
        //$this->link = $link;
        $this->body2 = $body2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.recoverypasswd')
            ->from('contacto@masterd.com.ve','Recupera tu clave T-Portal')
            ->subject($this->asunto);
    }
}
