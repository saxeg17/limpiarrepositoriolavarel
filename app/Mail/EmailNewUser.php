<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNewUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $mensaje;
    public $asunto;
    public $title1;
    public $body1;
    public $field1;
    public $field2;
    public function __construct($mensaje, $asunto, $body1, $field1, $field2, $title1)
    {
        //
        $this->mensaje = $mensaje;
        $this->asunto = $asunto;
        $this->title1 = $title1;
        $this->body1 = $body1;
        $this->field1 = $field1;
        $this->field2 = $field2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.newuser')
            ->from('contacto@masterd.com.ve','New User Emqu')
            ->subject($this->asunto);
    }
}
