-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-01-2017 a las 14:06:08
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `apirest_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_26_020537_create_video_table', 1),
(4, '2017_01_26_141208_create_note_table', 2),
(5, '2017_01_26_231700_add_column_rol_user', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notes`
--

INSERT INTO `notes` (`id`, `text`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Nota 1', 1, '2017-01-26 18:43:09', '2017-01-26 18:43:09'),
(2, 'Nota 2', 1, '2017-01-26 18:48:42', '2017-01-26 18:48:42'),
(3, 'Nota 3', 2, '2017-01-27 18:59:23', '2017-01-27 18:59:23'),
(4, 'Nota 4', 2, '2017-01-27 21:52:21', '2017-01-27 21:52:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`, `rol`) VALUES
(1, 'Ender Sanchez', 'ender@emqu.net', '$2y$10$lMJrxIKhEHcYzkmW82wxe.5mo.NXCfAGcvGqmXpnglWP3EXSUnhPK', 'HbrzIINo56JQHhsCSA2SjJwsbnIysgNlEiyrE5kAMkCjTq6n4SnJlnql4zJQ', 'LbQNtq33OW1szrVnweV3VWQMLGr90ggKStYXQ7r20gKILcoCYZVdaOfAPVT2', '2017-01-26 18:02:45', '2017-01-26 18:02:45', '1'),
(2, 'Juan Velazco', 'jc@emqu.net', '$2y$10$.AavB1pMxfhlcK34orasRuSipnZOvEVO8bfi.3oWrtsFVybRUd0mG', 'ky3ul3GrMBO89CNXhflMKeUM6crO4lZsrmoXY4SKnyuXxVqUcOogpNBjgbyc', '3sPN87whW3nupty1RayhAHtDtUjcI1I3Pah0NujJ9yghWOFMVoEDp7FVs6Cv', '2017-01-27 02:30:36', '2017-01-27 02:30:36', '2'),
(3, 'Hola', 'admin@t-systems.com', '$2y$10$ISb6MPTlxCcn83cEQ1QXUOlm..CcGPHUkRi1p1dK6jQJ.BFDj8UA6', 'P8AVUDnNG2fY8Z39hgI4vFBFHXYqIu4maaXjgIydUxtPynAB0auF7ibRzqmy', 'PX8CC8268zl3EZKPpDFndFbLG8yH2RTEN7KpLrNGIFFnSqfGpYX1j9b6xVtl', '2017-01-27 03:33:05', '2017-01-27 03:33:05', '4'),
(4, 'Alejandro', 'alejandro@t-systems.com', '$2y$10$FbVs69ZsW5xhwulzF4eQCOkugRDS/S7UOOjhU0zcIdsCFIQyUbr42', 'aNV54ntmMTKQ7kl4L8HFjmG2Tsf61u6uRU5GKldH3zW2Uqqfw5jEYy7mKshG', 'Bd9bJiF4TkgYRYZvwXdEQsjzskBhZw43Ryn7W1P8OX5KVv6NShCXSMCVa21w', '2017-01-27 03:34:35', '2017-01-27 03:34:35', '3');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notes_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
