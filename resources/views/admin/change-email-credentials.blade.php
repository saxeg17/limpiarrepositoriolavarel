@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Hola</div>
          <div class="panel-body">
            <form name="form-info" class="form-horizontal" role="form" action="{{ url('/edit') }}" method="POST">
              {!! csrf_field() !!}
              <div class="form-group">
                <label for="mail_username" class="col-lg-4 control-label">Mail Username:</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" id="mail_username" value="{{ env('MAIL_USERNAME') }}" name="mail_username">
                </div>
              </div>
              <div class="form-group">
                <label for="mail_password" class="col-lg-4 control-label">Mail Password :</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" id="mail_password" value="{{ env('MAIL_PASSWORD') }}" name="mail_password">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" name="boton" class="btn btn-primary botonConfirmEditInfo">Guardar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
