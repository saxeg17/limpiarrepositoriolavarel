<!doctype html>
<html>
<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>T-portal</title>
  <style>
    /* -------------------------------------
        GLOBAL RESETS
    ------------------------------------- */
    img {
      border: none;
      -ms-interpolation-mode: bicubic;
      max-width: 100%; }
    body {
      background-color: #f6f6f6;
      font-family: sans-serif;
      -webkit-font-smoothing: antialiased;
      font-size: 10px;
      line-height: 1.4;
      margin: 0;
      padding: 0;
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%; }
    table {
      border-collapse: separate;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
      width: 100%; }
    table td {
      font-family: sans-serif;
      font-size: 10px;
      vertical-align: top; }
    /* -------------------------------------
        BODY & CONTAINER
    ------------------------------------- */
    .body {
      background-color: #f6f6f6;
      width: 100%; }
    /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
    .container {
      display: block;
      Margin: 0 auto !important;
      /* makes it centered */
      max-width: 680px;
      padding: 10px;
      width: 680px; }
    /* This should also be a block element, so that it will fill 100% of the .container */
    .content {
      box-sizing: border-box;
      display: block;
      Margin: 0 auto;
      max-width: 680px;
      padding: 10px; }
    /* -------------------------------------
        HEADER, FOOTER, MAIN
    ------------------------------------- */
    .main {
      background: #fff;
      width: 100%;
      border: 1px solid #807d7d;}
    .wrapper {
      box-sizing: border-box;
      padding: 20px;
      padding-bottom: 0;}
    .footer {
      clear: both;
      padding-top: 10px;
      text-align: center;
      width: 100%; }
    .footer td,
    .footer p,
    .footer span,
    .footer a {
      color: #999999;
      font-size: 12px;
      text-align: center; }
    /* -------------------------------------
        TYPOGRAPHY
    ------------------------------------- */
    h1,
    h2,
    h3,
    h4 {
      color: #000000;
      font-family: sans-serif;
      font-weight: 400;
      line-height: 1.4;
      margin: 0;
      Margin-bottom: 30px; }
    h1 {
      font-size: 35px;
      font-weight: 300;
      text-align: center;
      text-transform: capitalize; }
    p,
    ul,
    ol {
      font-family: sans-serif;
      font-size: 12px;
      font-weight: normal;
      margin: 0;
      Margin-bottom: 15px; }
    p li,
    ul li,
    ol li {
      list-style-position: inside;
      margin-left: 5px; }
    a {
      color: #3498db;
      text-decoration: underline; }
    /* -------------------------------------
        BUTTONS
    ------------------------------------- */

    /* -------------------------------------
        OTHER STYLES THAT MIGHT BE USEFUL
    ------------------------------------- */
    .last {
      margin-bottom: 0; }
    .first {
      margin-top: 0; }
    .align-center {
      text-align: center; }
    .align-right {
      text-align: right; }
    .align-left {
      text-align: left; }
    .clear {
      clear: both; }
    .mt0 {
      margin-top: 0; }
    .mb0 {
      margin-bottom: 0; }
    .preheader {
      color: transparent;
      display: none;
      height: 0;
      max-height: 0;
      max-width: 0;
      opacity: 0;
      overflow: hidden;
      mso-hide: all;
      visibility: hidden;
      width: 0; }
    .powered-by a {
      text-decoration: none; }
    hr {
      border: 0;
      border-bottom: 1px solid #807d7d;
      Margin: 20px 0;
    }

    .titulo-derecha{
      float: right;
      text-transform: uppercase;
      font-weight: bold;
      color: #ef4194;
      padding-top: 10px;
      font-stretch: semi-condensed;
    }

    .titulo{
      color: #ef4194;
      font-weight: bold;
      font-stretch: semi-condensed;
      margin-bottom: 0;
      font-size: 14px;
    }

    p > span{
      color: #000;
      font-weight: initial;
      font-stretch: normal;
      font-size: 12px;
    }
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important; }
      table[class=body] p,
      table[class=body] ul,
      table[class=body] ol,
      table[class=body] td,
      table[class=body] span,
      table[class=body] a {
        font-size: 16px !important; }
      table[class=body] .wrapper,
      table[class=body] .article {
        padding: 10px !important; }
      table[class=body] .content {
        padding: 0 !important; }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important; }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important; }
      table[class=body] .btn table {
        width: 100% !important; }
      table[class=body] .btn a {
        width: 100% !important; }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important; }}
    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%; }
      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
        line-height: 100%; }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important; }
      .btn-primary table td:hover {
        background-color: #34495e !important; }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important; } }
  </style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body">
  <tr>
    <td>&nbsp;</td>
    <td class="container">
      <div class="content">

        <!-- START CENTERED WHITE CONTAINER -->
        <table class="main">

          <!-- START MAIN CONTENT AREA -->
          <tr>
            <td class="wrapper">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                    <img src="https://www.t-systems.com/blob/4868/5ee00b0443d42ef5998eef4b5721e293/t-systems-logo-data.png">
                    <div style="text-align: center; padding-top: 30px;">
                      <img src="http://tportal.emqu.net/img/emailtportal.png" alt="">
                    </div>
                    <br><br>
                    <p>Dear TSNA colleagues/ dear customer,</p>
                    <p>We would like to inform you about the successful on-boarding to T-Portal. The portal is fully operational right now.</p>
                    <p>You can reach your environment via:</p>
                    <br>
                    <p class="titulo">T-Portal</p>
                    <p class="titulo">Link: <a href="http://tmatrix.emqu.net/">http://tmatrix.emqu.net/</a></p>
                    <p class="titulo">User: <span><?php  echo $field1;  ?></span></p>
                    <p class="titulo">Password: <span><?php  echo $field2;  ?></span></p>

                    <p>T-Portal <?php  echo $title1;  ?> </p>
                    <p><br><?php echo $mensaje; ?></br></p>

                    <br><br>
                    <p>We wish lot of success using T-Portal</p>
                    <p>Sincerely yours,</p>
                    <p>T-Portal Team</p>
                    <hr>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
      </div>
    </td>
  </tr>
</table>
</body>
</html>
