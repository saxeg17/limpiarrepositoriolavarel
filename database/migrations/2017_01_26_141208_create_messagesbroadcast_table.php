<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesbroadcastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emqu_messagesbroadcast', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('message', 2000);
            $table->string('type');
            $table->string('icon');
            $table->integer('dias');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('emqu_messagesbroadcast_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_emqu_role')->unsigned();
            $table->integer('id_emqu_messagesbroadcast')->unsigned();
            $table->timestamps();

            $table->foreign('id_emqu_role')->references('id')->on('emqu_roles');
            $table->foreign('id_emqu_messagesbroadcast')->references('id')->on('emqu_messagesbroadcast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emqu_messagesbroadcast');

        Schema::dropIfExists('emqu_messagesbroadcast_roles');
    }
}
