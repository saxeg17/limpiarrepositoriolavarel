<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
 
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email',100)->unique();
            $table->string('api_token', 60)->unique();
            $table->string('password');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('position');
            $table->integer('id_emqu_role')->unsigned();
            $table->integer('id_emqu_company')->unsigned();
            $table->boolean('login');
            $table->foreign('id_emqu_role')->references('id')->on('emqu_roles');
            $table->foreign('id_emqu_company')->references('id')->on('emqu_company');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
