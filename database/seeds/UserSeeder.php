<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => '1',
            'name' => 'Admin',
            'email' => "admin@admin.com",
            'api_token' => str_random(60),
            'password' => bcrypt("admin123"),
            'firstname' => 'Admin',
            'lastname' => 'Admin',
            'position' => 'Position 1',
            'id_emqu_company' => 1,
            'id_emqu_role' => 2,
            'login' => false

        ]);
    }
}
