<?php

use Illuminate\Database\Seeder;
use App\roles;
class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        roles::create([
            'id' => '2',
			'role_name' => 'Admin'
        ]);
    }
}
