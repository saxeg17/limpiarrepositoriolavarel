<?php

use Illuminate\Database\Seeder;
use App\emqu_company;
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        emqu_company::create([
            'id' => '1',
			'name' => 'Empresa de prueba'
        ]);
    }
}
