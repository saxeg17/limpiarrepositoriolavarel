<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Mail\Message;
use App\Mail\Welcome as WelcomeEmail;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function (){
    Route::resource('user', 'UserController',
        ['only' => ['']]);
    Route::post('user/logout', 'Auth\AuthController@getLogout');
    

});

Route::group(['middleware' => 'auth.apikey'], function (){
    Route::post('user/isLogin', 'UserController@isLogin');
    Route::post('user', 'UserController@store');
    Route::get('user', 'UserController@index');
    Route::put('user/{id}', 'UserController@update');
    Route::get('user/data', 'UserController@showData');
    Route::post('email','SendEmails@send');
    Route::resource('roles', 'RolController');  
    Route::delete('user/{id}', 'UserController@destroy');
    Route::get('user/data/{id}', 'UserController@showUser');
    Route::post('user/passwd', 'UserController@changePasswd');

    /*Messages Broadcast*/
    Route::post('messages', 'MessagesBroadcastController@store');
    Route::get('messages', 'MessagesBroadcastController@allForUser');
    Route::get('messages/{id}', 'MessagesBroadcastController@show');
    /*Account Overview*/

    /*editar email credenciales*/
    Route::post('admin/editEmail', 'AdminController@editEmail');

});

Route::post('user/recoverycode', 'UserController@recoveryPasswd');
Route::post('user/verificationcode', 'UserController@recoveryPasswdByCode');
Route::post('user/changepasswdcode', 'UserController@recoveryChangePasswd');

Route::post('login', 'Auth\AuthController@postLogin');

